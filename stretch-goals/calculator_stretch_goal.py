def decimal_to_binary(decimal):
    if decimal > 1:
        return str(decimal_to_binary(decimal // 2)) + str(decimal % 2)
    else:
        return str(decimal % 2)


def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        return decimal_to_binary(a + b)
    elif operator == "-":
        return decimal_to_binary(a - b)
    elif operator == "*":
        return decimal_to_binary(a * b)
    elif operator == "/":
        return decimal_to_binary(int(a / b))
    else:
        return "Unrecognised operator"
    # ==============


print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
