def vowel_swapper(string):
    # ==============
    # Your code here
    swapped = string
    swapped = swapped.replace('a', '4')
    swapped = swapped.replace('A', '4')
    swapped = swapped.replace('e', '3')
    swapped = swapped.replace('E', '3')
    swapped = swapped.replace('i', '!')
    swapped = swapped.replace('I', '!')
    swapped = swapped.replace('o', 'ooo')
    swapped = swapped.replace('O', '000')
    swapped = swapped.replace('u', '|_|')
    swapped = swapped.replace('U', '|_|')
    return swapped
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console